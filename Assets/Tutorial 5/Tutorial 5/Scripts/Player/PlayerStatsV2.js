#pragma strict

var MaxHealth = 100;
var Health : int;
var maximumHitPoints = 100;
var hitPoints : float;
var regenerationSpeed : float = 5;

var damageSound : AudioClip[];
private var edamageSound : int = 0;

var playSoundsGO : GameObject;
var shakeGO : GameObject;
var die : AudioClip;
var deadReplacement : Transform;
var mySkin : GUISkin;
var canRegenerate : boolean = true;

private var radar : GameObject;
private var maxHitPoints : int;
var damageTexture : Texture;

private var time : float = 0.0;
private var alpha : float;
private var callFunction : boolean = false;
private var showTexture : boolean = false;
private var damageMessage : boolean = false;

public var healthBar : UnityEngine.UI.Image;
public var fillAmount : float;
public var powerUp : float;

function Start (temp)
{
	Health = MaxHealth;
	GUI.Box (new Rect (0,0,100,50), "Top-left");
	
}

function ApplyDammage (TheDammage : int)
{
	Health -= TheDammage;
    healthBar.fillAmount = (healthBar.fillAmount - 0.33);
		playSoundsGO.GetComponent.<AudioSource>().clip = damageSound[Random.Range(0, damageSound.length)];
		playSoundsGO.GetComponent.<AudioSource>().volume = 0.4;
		playSoundsGO.GetComponent.<AudioSource>().Play();
		time += TheDammage/7;

		if(Health <= 0) {
		  Dead();
	   }
}

function Dead()
{
	RespawnMenuV2.playerIsDead = true;
    healthBar.fillAmount = 0f;
	Debug.Log("Player Died");
}

function RespawnStats ()
{
	Health = MaxHealth;
    healthBar.fillAmount = 1f;
}

function OnTriggerEnter (other : Collider) {
    if ( healthBar.fillAmount != 1) {
        Health += powerUp; 
        healthBar.fillAmount = (healthBar.fillAmount + 0.33);
    }
}
