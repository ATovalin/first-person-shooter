#pragma strict

var Health = 100;
var explosion : Transform;

function ApplyDammage (TheDammage : int)
{
	Health -= TheDammage;
	
	if(Health <= 0)
	{
		Dead();
	}
}

function Dead()
{
	var exp = Instantiate(explosion, gameObject.transform.position, Quaternion.identity);
    Destroy (gameObject);
}